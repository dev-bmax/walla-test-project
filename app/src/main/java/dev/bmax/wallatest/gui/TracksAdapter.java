package dev.bmax.wallatest.gui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import dev.bmax.data.TrackInfo;
import dev.bmax.wallatest.R;
import static dev.bmax.wallatest.data.Constants.*;

/**
 * This class supplies the individual items of the underlying recycler view.
 */
public class TracksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    /**
     * The receiver of the item click event.
     */
    public interface ListItemClickListener {
        /**
         * Called when a list item is clicked.
         * @param position - the position of the clicked item.
         */
        void onListItemClick(int position);
    }

    /**
     * The possible view types used in the recycler view.
     */
    enum ViewType {
        TRACK_MP3(0),
        TRACK_RAW(1),
        TRACK_WAV(2),
        TRACK_OTHER(3),
        TRACK_GRID(4),
        PROGRESS_INDICATOR(5);

        ViewType(int id) {
            this.id = id;
        }

        final int id;
    }

    /**
     * This class holds the references to the views inside a
     * list item of type TRACK_*.
     */
    public static class ListViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_track_artwork)
        ImageView imgTrackArtwork;
        @Bind(R.id.txt_track_title)
        TextView txtTrackTitle;
        private View trackView;

        public ListViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            this.trackView = view;
        }
    }

    /**
     * This class holds the references to the views inside a
     * grid item of type TRACK_*.
     */
    public static class GridViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_track_artwork)
        ImageView imgTrackArtwork;
        private View trackView;

        public GridViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            this.trackView = view;
        }
    }

    /**
     * This class could hold the references to the views inside a
     * list item of type PROGRESS_INDICATOR. But in this implementation
     * there is no need to use them.
     */
    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressViewHolder(View view) {
            super(view);
        }
    }

    /**
     * Public constructor.
     * @param trackInfoList - the list of track info objects to be used
     *                        when populating the recycler view
     * @param context - needed for the Picasso library
     * @param clickListener - the receiver of the item click event
     */
    public TracksAdapter(List<TrackInfo> trackInfoList, Context context,
                         ListItemClickListener clickListener) {
        this.trackInfoList = trackInfoList;
        this.context = context;
        this.clickListener = clickListener;
    }

    /**
     * Replaces the current data set with the provided one.
     * @param trackInfoList - the new list of track info objects to be
     *                        used when populating the recycler view
     */
    public void updateTrackInfos(List<TrackInfo> trackInfoList) {
        this.trackInfoList = trackInfoList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder holder;

        if (viewType == ViewType.TRACK_MP3.id) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_track_mp3, parent, false);
            holder = new ListViewHolder(view);
        } else if (viewType == ViewType.TRACK_RAW.id) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_track_raw, parent, false);
            holder = new ListViewHolder(view);
        } else if (viewType == ViewType.TRACK_WAV.id) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_track_wav, parent, false);
            holder = new ListViewHolder(view);
        } else if (viewType == ViewType.TRACK_OTHER.id) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_track_other, parent, false);
            holder = new ListViewHolder(view);
        } else if (viewType == ViewType.TRACK_GRID.id) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.grid_item_track, parent, false);
            holder = new GridViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_progress, parent, false);
            holder = new ProgressViewHolder(view);
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,
                                 final int position) {
        if (holder instanceof ListViewHolder) {
            ListViewHolder lvh = (ListViewHolder) holder;
            TrackInfo trackInfo = trackInfoList.get(position);

            Picasso.with(context)
                    .load(trackInfo.getArtworkUrl())
                    .placeholder(R.drawable.sound_cloud)
                    .into(lvh.imgTrackArtwork);

            String text = String.format("%s (%s)",
                    trackInfo.getTitle(), trackInfo.getFormat());
            lvh.txtTrackTitle.setText(text);

            lvh.trackView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onListItemClick(position);
                }
            });
        } else if (holder instanceof GridViewHolder) {
            GridViewHolder gvh = (GridViewHolder) holder;
            TrackInfo trackInfo = trackInfoList.get(position);

            Picasso.with(context)
                    .load(trackInfo.getArtworkUrl())
                    .placeholder(R.drawable.sound_cloud)
                    .into(gvh.imgTrackArtwork);

            gvh.trackView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onListItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return trackInfoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        TrackInfo info = trackInfoList.get(position);
        if (info == null) {
            return ViewType.PROGRESS_INDICATOR.id;
        }

        switch (currentMode) {
            case LIST:
                String format = info.getFormat().toLowerCase();
                if (Formats.MP3.contains(format)) {
                    return ViewType.TRACK_MP3.id;
                } else if (Formats.WAV.contains(format)) {
                    return ViewType.TRACK_WAV.id;
                } else if (Formats.RAW.contains(format)) {
                    return ViewType.TRACK_RAW.id;
                } else {
                    return ViewType.TRACK_OTHER.id;
                }
            case GRID:
                return ViewType.TRACK_GRID.id;
            default:
                /**
                 * Should never happen.
                 */
                return -1;
        }
    }

    /**
     * Adds a list item at the bottom of the recycler view,
     * that shows a progress wheel.
     */
    public void addProgressIndicator() {
        trackInfoList.add(null);
        notifyItemInserted(trackInfoList.size() - 1);
    }

    /**
     * Removes the progress wheel list item from the bottom
     * of the recycler view.
     */
    public void removeProgressIndicator() {
        trackInfoList.remove(trackInfoList.size() - 1);
        notifyItemRemoved(trackInfoList.size() - 1);
    }

    public void setViewMode(SearchActivity.ViewMode mode) {
        currentMode = mode;
    }
    /**
     * Private zone:
     */
    private List<TrackInfo> trackInfoList;
    private Context context;
    private ListItemClickListener clickListener;
    private SearchActivity.ViewMode currentMode;
}
