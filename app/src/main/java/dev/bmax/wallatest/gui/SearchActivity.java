package dev.bmax.wallatest.gui;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import dev.bmax.data.TrackInfo;
import dev.bmax.data.TracksFetcher;
import dev.bmax.wallatest.R;
import dev.bmax.wallatest.data.PrefsHelper;
import dev.bmax.wallatest.data.SearchTask;
import dev.bmax.wallatest.logic.NetworkUtils;

import static dev.bmax.wallatest.data.Constants.*;

/**
 * This activity prompts the user to search tracks on SoundCloud.
 */
public class SearchActivity extends AppCompatActivity implements
        SearchTask.SearchResultsListener,
        TracksAdapter.ListItemClickListener {
    @Bind(R.id.search_activity_root_view)
    LinearLayout rootView;

    @Bind(R.id.search_activity_search_view)
    SearchView searchView;

    @Bind(R.id.search_activity_progress_bar)
    ContentLoadingProgressBar progressBar;

    @Bind(R.id.search_activity_recycler_view)
    RecyclerView recyclerView;

    /**
     * The possible modes of operation.
     */
    enum ViewMode {
        /**
         * Results are shown as a list.
         */
        LIST(Values.VIEW_MODE_LIST),
        /**
         * Results are shown as a grid.
         */
        GRID(Values.VIEW_MODE_GRID);

        ViewMode(String id) {
            this.id = id;
        }

        final String id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        /**
         * Grab the views references.
         */
        ButterKnife.bind(this);
        setupSearchView();
        /**
         * Try to read the stored view mode.
         */
        String storedModeId = PrefsHelper.getInstance().getString(Keys.VIEW_MODE);
        if (ViewMode.GRID.id.equals(storedModeId)) {
            setupRecyclerView(ViewMode.GRID);
        } else {
            setupRecyclerView(ViewMode.LIST);
        }
        setupTracksFetcher();
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        /**
         * SearchView widget delivers the user's search query
         * packed in an Intent, and since this activity has a
         * launch mode option of single top, this is where we
         * handle the search input.
         */
        setIntent(intent);
        handleIntent(intent);
    }

    @Override
    public void onResultsReady(List<TrackInfo> freshTracks) {
        if (isFinishing()) {
            return;
        }

        if (loadingMore) {
            /**
             * More results of the last search have arrived,
             * recycler view and it's adapter need to be updated.
             */
            tracksAdapter.removeProgressIndicator();
            trackInfoList.addAll(freshTracks);
            tracksAdapter.updateTrackInfos(trackInfoList);
            tracksAdapter.notifyDataSetChanged();
            recyclerView.addOnScrollListener(new LoadMoreScrollListener());
            loadingMore = false;
            /**
             * Now that the results are available we can
             * safely increment the current page number.
             */
            currentPage++;
        } else {
            /**
             * Results of a new search have arrived,
             * supply them to the recycler view.
             */
            trackInfoList = freshTracks;
            tracksAdapter = new TracksAdapter(trackInfoList, this, this);
            tracksAdapter.setViewMode(currentMode);
            recyclerView.setAdapter(tracksAdapter);
            progressBar.setVisibility(View.INVISIBLE);
            /**
             * Since this is a new search, restore the original
             * current page number.
             */
            currentPage = 1;
        }
    }

    @Override
    public void onNothingFound() {
        if (isFinishing()) {
            return;
        }

        if (loadingMore) {
            /**
             * More results of the last search are not available.
             */
            tracksAdapter.removeProgressIndicator();
            loadingMore = false;
        } else {
            /**
             * Results of the new search are not available.
             */
            progressBar.setVisibility(View.INVISIBLE);
        }

        Toast.makeText(
                this,
                getString(R.string.nothing_found),
                Toast.LENGTH_SHORT
        ).show();
    }

    @Override
    public void onListItemClick(int position) {
        startTrackActivity(position);
    }

    /**
     * Called when one of the mode buttons is selected
     * (list mode or grid mode)
     * @param view - the clicked view
     */
    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.search_activity_radio_list:
                if (checked) {
                    setupRecyclerView(ViewMode.LIST);
                    PrefsHelper.getInstance().putString(Keys.VIEW_MODE,
                            ViewMode.LIST.id);
                }
                break;
            case R.id.search_activity_radio_grid:
                if (checked) {
                    setupRecyclerView(ViewMode.GRID);
                    PrefsHelper.getInstance().putString(Keys.VIEW_MODE,
                            ViewMode.GRID.id);
                }
                break;
        }
    }

    /**
     * Private zone:
     */
    private TracksAdapter tracksAdapter;
    private TracksFetcher tracksFetcher;
    private CharSequence searchText;
    private List<TrackInfo> trackInfoList;
    private ViewMode currentMode;
    private int currentPage = 1;
    private volatile boolean loadingMore;

    private void setupTracksFetcher() {
        tracksFetcher = new TracksFetcher();
        tracksFetcher.setSearchUrl(SEARCH_URL);
        tracksFetcher.setClientId(CLIENT_ID);
    }

    private void setupRecyclerView(ViewMode mode) {
        currentMode = mode;
        /**
         * Update the adapter if it exists.
         */
        if (tracksAdapter != null) {
            tracksAdapter.setViewMode(mode);
        }
        recyclerView.setHasFixedSize(true);
        /**
         * Choose the layout manager.
         */
        RecyclerView.LayoutManager layoutManager = null;
        switch (mode) {
            case LIST:
                layoutManager = new LinearLayoutManager(this);
                break;
            case GRID:
                layoutManager = new GridLayoutManager(this, 3);
                ItemOffsetDecoration itemDecoration =
                        new ItemOffsetDecoration(this, R.dimen.grid_layout_item_offset);
                recyclerView.addItemDecoration(itemDecoration);
                break;
        }
        recyclerView.setLayoutManager(layoutManager);
        /**
         * Update the custom scroll listener.
         */
        recyclerView.addOnScrollListener(new LoadMoreScrollListener());
    }

    private void setupSearchView() {
        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            startSearchAsync(query);
        }
    }

    private void startSearchAsync(String query) {
        searchText = query;
        if (NetworkUtils.isOnline(this)) {
            progressBar.setVisibility(View.VISIBLE);
            new SearchTask(tracksFetcher, this, searchText)
                    .execute(currentPage);
        } else {
            showSnackbar(R.string.connection_unavailable);
        }
    }

    private void loadMoreResults() {
        if (NetworkUtils.isOnline(this)) {
            loadingMore = true;
            tracksAdapter.addProgressIndicator();
            new SearchTask(tracksFetcher, this, searchText)
                    .execute(currentPage + 1);
        } else {
            showSnackbar(R.string.connection_unavailable);
        }
    }

    private void startTrackActivity(int position) {
        Intent intent = new Intent(getApplicationContext(), TrackActivity.class);
        /**
         * Pass the track list to the next activity, casting if
         * necessary.
         */
        if (trackInfoList instanceof ArrayList) {
            intent.putExtra(Keys.TRACK_INFO_LIST, (ArrayList) trackInfoList);
        } else {
            intent.putExtra(Keys.TRACK_INFO_LIST, new ArrayList<>(trackInfoList));
        }
        intent.putExtra(Keys.CURRENT_TRACK_POS, position);
        startActivity(intent);
    }

    private void showSnackbar(int messageRes) {
        Snackbar.make(rootView, messageRes, Snackbar.LENGTH_SHORT)
                .show();
    }
    /**
     * This custom listener triggers loading more results when the
     * user scrolls to the end of the list, unless the current page
     * is the maximum allowed (since there is a limit on the number
     * of result pages to show).
     */
    private class LoadMoreScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            LinearLayoutManager layoutManager = (LinearLayoutManager)
                    recyclerView.getLayoutManager();
            int totalItemCount = layoutManager.getItemCount();
            int lastVisibleItem = layoutManager.findLastVisibleItemPosition();
            if (!loadingMore && totalItemCount <= (lastVisibleItem + THRESHOLD)
                    && currentPage < MAX_PAGES) {
                loadMoreResults();
            }
        }
    }
}
