package dev.bmax.wallatest.gui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import dev.bmax.data.TrackInfo;
import dev.bmax.wallatest.R;
import dev.bmax.wallatest.logic.PlayerService;

import static dev.bmax.wallatest.data.Constants.*;

/**
 * This activity presents the audio player controls.
 */
public class TrackActivity extends AppCompatActivity {
    @Bind(R.id.track_activity_img_artwork)
    ImageView imgArtwork;

    @Bind(R.id.track_activity_btn_play)
    ImageView btnPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);
        /**
         * Grab the views references.
         */
        ButterKnife.bind(this);
        extractTrackList();
        TrackInfo info = trackInfoList.get(curTrackPos);
        setTrackImage(info.getArtworkUrl());
    }

    private void extractTrackList() {
        Bundle extras = getIntent().getExtras();
        /**
         * The cast from Serializable to ArrayList is safe,
         * since the type check has been done in the sending
         * activity.
         */
        trackInfoList = (ArrayList<TrackInfo>)
                extras.getSerializable(Keys.TRACK_INFO_LIST);
        curTrackPos = extras.getInt(Keys.CURRENT_TRACK_POS);
    }

    private void setTrackImage(String artworkUrl) {
        Picasso.with(this)
               .load(artworkUrl)
               .placeholder(R.drawable.sound_cloud)
               .into(imgArtwork);
    }

    public void onButtonPrevClicked(View view) {
        if (curTrackPos > 0) {
            Intent playerIntent =
                    new Intent(getApplicationContext(), PlayerService.class);
            TrackInfo info = trackInfoList.get(--curTrackPos);
            /**
             * If playing a track already, stop it first.
             */
            if (playing) {
                stopService(playerIntent);
                playing = false;
                /**
                 * And start the player service passing it the
                 * new audio stream URL.
                 */
                playerIntent.setAction(PlayerService.ACTION_PLAY);
                playerIntent.putExtra(Keys.TARGET_TRACK_URL,
                        info.getStreamUrl() + BRIDGE + CLIENT_ID);
                startService(playerIntent);
                playing = true;
            }
            /**
             * Update the image.
             */
            setTrackImage(info.getArtworkUrl());
        }
    }

    public void onButtonPlayClicked(View view) {
        Intent playerIntent =
                new Intent(getApplicationContext(), PlayerService.class);
        if (playing) {
            /**
             * Stop the current track first.
             */
            stopService(playerIntent);
            playing = false;
            togglePlayButtonIcon();
        } else {
            /**
             * Start the player service passing it the
             * audio stream URL.
             */
            TrackInfo info = trackInfoList.get(curTrackPos);
            playerIntent.setAction(PlayerService.ACTION_PLAY);
            playerIntent.putExtra(Keys.TARGET_TRACK_URL,
                    info.getStreamUrl() + BRIDGE + CLIENT_ID);
            startService(playerIntent);
            playing = true;
            togglePlayButtonIcon();
        }
    }

    public void onButtonNextClicked(View view) {
        if (curTrackPos < trackInfoList.size()-1) {
            Intent playerIntent =
                    new Intent(getApplicationContext(), PlayerService.class);
            TrackInfo info = trackInfoList.get(++curTrackPos);
            /**
             * If playing a track already, stop it first.
             */
            if (playing) {
                stopService(playerIntent);
                playing = false;
                /**
                 * Start the player service passing it the
                 * new audio stream URL.
                 */
                playerIntent.setAction(PlayerService.ACTION_PLAY);
                playerIntent.putExtra(Keys.TARGET_TRACK_URL,
                        info.getStreamUrl() + BRIDGE + CLIENT_ID);
                startService(playerIntent);
                playing = true;
            }
            /**
             * Update the image.
             */
            setTrackImage(info.getArtworkUrl());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /**
         * Stop the current track if needed.
         */
        Intent playerIntent =
                new Intent(getApplicationContext(), PlayerService.class);
        if (playing) {
            stopService(playerIntent);
            playing = false;
        }
    }

    private void togglePlayButtonIcon() {
        if (playing) {
            btnPlay.setImageResource(android.R.drawable.ic_media_pause);
        } else {
            btnPlay.setImageResource(android.R.drawable.ic_media_play);
        }
    }

    private List<TrackInfo> trackInfoList;
    private int curTrackPos;
    private volatile boolean playing;
}
