package dev.bmax.wallatest.logic;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import dev.bmax.wallatest.R;
import dev.bmax.wallatest.data.Constants;

/**
 */
public class PlayerService extends Service
        implements MediaPlayer.OnPreparedListener{

    public static final String ACTION_PLAY = "dev.bmax.wallatest.PLAY";
    public static boolean running;

    @Override
    public void onCreate() {
        super.onCreate();
        running = true;
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                PlayerService.class.getSimpleName());
        wakeLock.acquire();
        startForeground(1234, makeFixedNotification());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (player.isPlaying()) {
            player.stop();
        }
        player.release();
        wakeLock.release();
        running = false;
        stopForeground(true);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals(ACTION_PLAY)) {
            String url = intent.getStringExtra(Constants.Keys.TARGET_TRACK_URL);
            try {
                player = new MediaPlayer();
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                player.setDataSource(url);
                player.setOnPreparedListener(this);
                player.prepareAsync();
            } catch (Exception e) {
                Toast.makeText(
                        getApplicationContext(),
                        e.getLocalizedMessage(),
                        Toast.LENGTH_LONG
                ).show();
            }
        }
        return START_STICKY;
    }

    @Nullable @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        player.start();
    }

    /**
     * Private zone:
     */
    private PowerManager.WakeLock wakeLock;
    private MediaPlayer player;

    private Notification makeFixedNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.ic_media_play)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.playing_track));
        return builder.build();
    }
}
