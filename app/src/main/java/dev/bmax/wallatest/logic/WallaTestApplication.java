package dev.bmax.wallatest.logic;

import android.app.Application;
import android.content.Context;

/**
 * This class is instantiated when the process for our application
 * is created and caches the reference to itself in a static field
 * for easy access.
 */
public class WallaTestApplication extends Application {
    private static Context applicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = this;
    }

    /**
     * @return - The application context
     */
    public static Context getContext() {
        return applicationContext;
    }
}
