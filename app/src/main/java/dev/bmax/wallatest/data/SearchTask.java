package dev.bmax.wallatest.data;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.List;

import dev.bmax.data.TrackInfo;
import dev.bmax.data.TracksFetcher;

/**
 * This custom async task invokes the search method of the provided
 * tracks fetcher on the worker thread and returns the result to the
 * provided listener. The integer parameter (expected by the execute
 * method) is the page number which is converted into an offset value
 * and supplied to the fetcher together with the search text and the
 * fixed page size.
 */
public class SearchTask extends AsyncTask<Integer, Void, List<TrackInfo>> {
    /**
     * The receiver of the search results.
     */
    public interface SearchResultsListener {
        /**
         * Called when a not empty set of results is ready.
         * @param feedItems - the tracks that match the search
         */
        void onResultsReady(List<TrackInfo> feedItems);
        /**
         * Called if no results were received.
         */
        void onNothingFound();
    }

    /**
     * Public constructor.
     * @param fetcher - the class that executes the network request.
     * @param listener - the receiver of the search results
     * @param searchText - the search query string
     */
    public SearchTask(@NonNull TracksFetcher fetcher,
                      SearchResultsListener listener,
                      CharSequence searchText) {
        this.fetcher = fetcher;
        this.listener = listener;
        this.searchText = searchText;
    }

    @Override
    protected List<TrackInfo> doInBackground(Integer... params) {
        int pageNum = params[0];
        int pageOffset = pageNum * Constants.PAGE_SIZE;
        return fetcher.search(
                searchText.toString(),
                Constants.PAGE_SIZE,
                pageOffset
        );
    }

    @Override
    protected void onPostExecute(List<TrackInfo> trackInfoList) {
        if (listener != null) {
            if (trackInfoList != null && trackInfoList.size() > 0) {
                listener.onResultsReady(trackInfoList);
            } else {
                listener.onNothingFound();
            }
        }
    }

    /**
     * Private zone:
     */
    private TracksFetcher fetcher;
    private SearchResultsListener listener;
    private CharSequence searchText;
}
