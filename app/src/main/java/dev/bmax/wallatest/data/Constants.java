package dev.bmax.wallatest.data;

import java.util.HashSet;
import java.util.Set;

/**
 * This class contains some useful values that are referenced
 * from all parts of the project.
 */
public class Constants {
    /**
     * The values to use when nothing else is specified.
     */
    public static class DefaultValues {
        public static final String STRING = "";
        public static final int INT = 0;
        public static final boolean BOOLEAN = false;
        public static final Set<String> STRING_SET = new HashSet<>();
    }

    /**
     * Keys used with the intent extras bundle.
     */
    public static class Keys {
        public static final String TRACK_INFO_LIST = "track_info_list";
        public static final String CURRENT_TRACK_POS = "current_track_position";
        public static final String TARGET_TRACK_URL = "target_track_url";
        public static final String VIEW_MODE = "recycler_view_mode";
    }

    /**
     * Values used with the persistent storage.
     */
    public static class Values {
        public static final String VIEW_MODE_LIST = "view_mode_list";
        public static final String VIEW_MODE_GRID = "view_mode_grid";
    }

    public static class Formats {
        public static final String MP3 = "mp3";
        public static final String WAV = "wav";
        public static final String RAW = "raw";
    }

    public static final String PREFERENCES_NAME = "global_preferences";
    public static final String SEARCH_URL = "https://api.soundcloud.com/tracks?";
    public static final String BRIDGE = "?client_id=";
    public static final String CLIENT_ID = "d652006c469530a4a7d6184b18e16c81";

    public static final int PAGE_SIZE = 20;
    public static final int MAX_PAGES = 5;
    public static final int THRESHOLD = 2;
}
