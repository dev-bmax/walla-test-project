package dev.bmax.wallatest.data;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;
import java.util.Set;

import dev.bmax.wallatest.logic.WallaTestApplication;
import static dev.bmax.wallatest.data.Constants.*;

/**
 * This class helps to deal with the shared preferences storage.
 */
public class PrefsHelper {
    /**
     * @return - The one and only instance of PrefsHelper
     */
    public static PrefsHelper getInstance() {
        return INSTANCE;
    }

    /**
     * Retrieve a String value from the preferences.
     */
     public String getString(String key) {
        return preferences.getString(key, DefaultValues.STRING);
    }

    /**
     * Retrieve a boolean value from the preferences.
     */
    public boolean getBoolean(String key) {
        return preferences.getBoolean(key, DefaultValues.BOOLEAN);
    }

    /**
     * Retrieve an int value from the preferences.
     */
    public int getInt(String key) {
        return preferences.getInt(key, DefaultValues.INT);
    }

    /**
     * Retrieve a long value from the preferences.
     */
    public long getLong(String key) {
        return preferences.getLong(key, (long) DefaultValues.INT);
    }

    /**
     * Retrieve a float value from the preferences.
     */
    public float getFloat(String key) {
        return preferences.getFloat(key, (float) DefaultValues.INT);
    }

    /**
     * Retrieve a Set of String value from the preferences.
     */
    public Set<String> getStringSet(String key) {
        return preferences.getStringSet(key, DefaultValues.STRING_SET);
    }

    /**
     * Retrieve all values from the preferences.
     */
    public Map<String, ?> getAll() {
        return preferences.getAll();
    }

    /**
     * Place a boolean value into the preferences.
     */
    public void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * Place an int value into the preferences.
     */
    public void putInt(String key, int value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * Place a long value into the preferences.
     */
    public void putLong(String key, long value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    /**
     * Place a float value into the preferences.
     */
    public void putFloat(String key, float value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    /**
     * Place a String value into the preferences.
     */
    public void putString(String key, String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Place a Set of String value into the preferences.
     */
    public void putStringSet(String key, Set<String> value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putStringSet(key, value);
        editor.apply();
    }

    /**
     * Private zone:
     */
    private static final PrefsHelper INSTANCE = new PrefsHelper(
            WallaTestApplication.getContext(), PREFERENCES_NAME);
    private SharedPreferences preferences;

    /**
     * Private constructor
     */
    private PrefsHelper(Context context, String fileName) {
        this.preferences = context.getSharedPreferences(
                fileName, Context.MODE_PRIVATE);
    }
}
