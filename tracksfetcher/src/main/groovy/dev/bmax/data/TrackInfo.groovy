package dev.bmax.data

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode

/**
 * This is my Groovy bean. Note that the public constructor
 * and all of the setters/getters are generated automatically
 * at compile time.
 */
@CompileStatic(TypeCheckingMode.SKIP)
public class TrackInfo implements Serializable {
    static final long serialVersionUID = 0L
    String title
    String artworkUrl
    String streamUrl
    String format
}