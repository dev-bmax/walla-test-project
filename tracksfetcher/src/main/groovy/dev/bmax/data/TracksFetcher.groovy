package dev.bmax.data

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.json.JsonSlurper

/**
 * This class executes the search request, building the URL string
 * from the given parameters. Then, it parses the resulting JSON object
 * into a list of TrackInfo container objects and returns that list.
 */
@CompileStatic(TypeCheckingMode.SKIP)
public class TracksFetcher {
    String searchUrl
    String clientId

    List<TrackInfo> search(String searchText, int pageSize, int pageOffset) {
        def tracks = []
        String encoded = URLEncoder.encode(searchText, 'UTF-8')
        def params = [q:encoded, client_id:clientId, limit:pageSize, offset:pageOffset]
        String query = params.collect { k,v -> "$k=$v" }.join('&')
        def url = searchUrl + query
        def json = new JsonSlurper().parseText(url.toURL().text)

        for (track in json) {
            def trackInfo = [
                    title: track.title,
                    artworkUrl: track.artwork_url,
                    streamUrl: track.stream_url,
                    format: track.original_format
            ] as TrackInfo

            tracks << trackInfo
        }

        tracks
    }
}
